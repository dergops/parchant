stages:
  - prebuild
  - tagging
  - build
  - release

variables:
  LINUX_TARGET: x86_64-unknown-linux-gnu
  MUSL_TARGET: x86_64-unknown-linux-musl
  WINDOWS_TARGET: x86_64-pc-windows-gnu

  LINUX_BINARY: parchant-linux-amd64
  MUSL_BINARY: parchant-linux-musl
  WINDOWS_BINARY: parchant-windows-amd64.exe

  BASE_TARGET_PATH: target/


prebuilding:
  stage: prebuild
  image: rust:slim-buster
  rules:
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - rustup target add $LINUX_TARGET
  script:
    - cargo build --target-dir $BASE_TARGET_PATH --target $LINUX_TARGET --release
  after_script:
    - mkdir binaries
    - cp $BASE_TARGET_PATH$LINUX_TARGET/release/parchant binaries/$LINUX_BINARY
  artifacts:
    paths:
      - binaries
    expire_in: 1h


tagging:
  image: debian:buster-slim
  stage: tagging
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - apt-get update -qy && apt-get install openssh-client git -qqy
    - eval `ssh-agent -s`
    - mkdir -p /root/.ssh
    - chmod 700 /root/.ssh
    - echo "$DEPLOY_KEY_PRIVATE" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 600 /root/.ssh/id_rsa
    - ssh-add /root/.ssh/id_rsa
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - export RELEASE_TAG=$(./binaries/$LINUX_BINARY tag CHANGELOG.md)
  script:
    - git config --global user.email "david@vrtel.dev"
    - git config --global user.name "David Vrtěl"
    - git config --global url."ssh://git@gitlab.com/".insteadOf "https://github.com/"
    - git remote set-url origin git@gitlab.com:davidvrtel/parchant.git
    - git tag -d $(git tag -l)
    - git tag ${RELEASE_TAG}
    - git push origin ${RELEASE_TAG}


binaries:
  stage: build
  image: rust:slim-buster
  rules:
      - if: $CI_COMMIT_TAG != null
  before_script:
    - apt update && apt install gcc-mingw-w64-x86-64 -y
    - rustup target add x86_64-pc-windows-gnu $LINUX_TARGET $MUSL_TARGET $WINDOWS_TARGET

  script:
    - cargo build --target-dir $BASE_TARGET_PATH --target $LINUX_TARGET --release
    - cargo build --target-dir $BASE_TARGET_PATH --target $MUSL_TARGET --release
    - cargo build --target-dir $BASE_TARGET_PATH --target $WINDOWS_TARGET --release

  after_script:
    - mkdir binaries
    - cp $BASE_TARGET_PATH$LINUX_TARGET/release/parchant binaries/$LINUX_BINARY
    - cp $BASE_TARGET_PATH$MUSL_TARGET/release/parchant binaries/$MUSL_BINARY
    - cp $BASE_TARGET_PATH$WINDOWS_TARGET/release/parchant.exe binaries/$WINDOWS_BINARY

  artifacts:
    name: parchant-binaries
    paths:
      - binaries
    expire_in: never


publishing-release:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  needs:
    - job: binaries
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG != null
  before_script:
    - ls -alF binaries/
    - chmod +x binaries/$MUSL_BINARY

  script:
    - binaries/$MUSL_BINARY desc CHANGELOG.md > release_notes.txt
    - echo "Releasing version $CI_COMMIT_TAG"
  release:
    name: "Release $CI_COMMIT_TAG"
    description: "./release_notes.txt"
    tag_name: $CI_COMMIT_TAG
    ref: $CI_COMMIT_TAG
    assets:
      links:
        - name: $LINUX_BINARY
          url: https://gitlab.com/davidvrtel/parchant/-/jobs/artifacts/$CI_COMMIT_TAG/raw/binaries/$LINUX_BINARY?job=binaries
          link_type: 'package'
        - name: $MUSL_BINARY
          url: https://gitlab.com/davidvrtel/parchant/-/jobs/artifacts/$CI_COMMIT_TAG/raw/binaries/$MUSL_BINARY?job=binaries
          link_type: 'package'
        - name: $WINDOWS_BINARY
          url: https://gitlab.com/davidvrtel/parchant/-/jobs/artifacts/$CI_COMMIT_TAG/raw/binaries/$WINDOWS_BINARY?job=binaries
          link_type: 'package'
