## v0.3.2 (8-Feb-2022)

### New features

- Updated crates and switch to 2021 edition.
- Automated releases of package in GitLab

## v0.3.1 (15-Jun-2021)

### New features

- Added proper error handling for files that doesn't exist

## v0.3.0 (14-Jun-2021)

### New features

- Rewritten with different crate, structopt
- Can now display help and version with `-h` and `-V` flags
- More descriptive stderr

## v0.2.2 (26-Jan-2021)

### New features

- Implements reading from stdin

## v0.2.1 (26-Jan-2021)

### New features

- Changes the Config to use PathBuf for handling the filepath

## v0.2.0 (15-Jan-2021)

### New features

- Added third optional parameter to specify pattern to match at the beginning of line
- Better error messages for different cases.

## v0.1.0 (14-Jan-2021)

### New features

- Added two arguments for parsing changelogs specification
- Parsing of file which name is suplied in second argument
- First argument is either `tags` or `desc` based on latest tag or latest description
