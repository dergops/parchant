use std::process;

use parchant::Cli;
use structopt::StructOpt;

fn main() {
  let args = Cli::from_args();
  // println!("{:#?}", config);
  if let Err(e) = parchant::run(args) {
    eprintln!("error: {}", e);
    process::exit(1);
  }
}
