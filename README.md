# ParChanT - Parsing Changelog Tool

ParChanT is tool to be used mainly for my own need to print the last release info from CHANGELOG.md which keeps all the version changes. I am using it with GitLab CI to push tags and create releases.

## How to use

### Build

Using `rustup` and `cargo` is prefered.

Execute following command to built binary into target/release folder.
```
cargo build --release
```

If you want to use tool on Alpine, the binary needs statically linked binaries in order for it to work on every Alpine linux. Use following target instead.
```
rustup target add x86_64-unknown-linux-musl
cargo build --target x86_64-unknown-linux-musl --release
```
Place the binary in `/usr/bin/`, `/usr/local/bin/` or some other PATH to use it as a command anywhere.

### Command
```
parchant {tag|desc} FILE [PATTERN]
```
From `FILE` in current directory, get tag of top listed release or description for that release and print it out.

It looks for specified `PATTERN` (by default it's `"## v"`) at the beginning of each line. After it gets the first one, output differs based on first argument being `tag` or `desc`.

`tag` splits the first matched line by whitespace and prints out second item.

`desc` prints out everything between first and second (if no second, prints the rest of the file) line, first line included.

If you don't know what to do, you can always ask for `--help`.

### Examples

Consider following markdown file. This is the ideal format that you can use with ParChanT.

```md
# CHANGELOG

This is changelog with all released versions and changes.

## v0.1.0 (14-Jan-2021)

### New features

- Description
- of
- current release

### Bug fixes

- none

## v0.0.0 (1-Jan-2021)

### New features

- none

```
-----
ParChanT can print out tag of last command with following command.
```bash
parchant tag CHANGELOG.md
```
Prints
```
v0.1.0
```
-----

To print out entire description of last release run following command.
```bash
parchant desc CHANGELOG.md
```
Prints
```
## v0.1.0 (14-Jan-2021)

### New features

- Description
- of
- current release

### Bug fixes

- none

```
-----
If you need to match other pattern than default `"## v"`, you can specify it as a third argument within double quotes.

For example your heading for release looks like this
`## 0.5.6 version`.
```bash
parchant tag CHANGELOG.md "## "
```

Note you can't go wild the paterns, it still extracts set of characters after first whitespace, so for heading like `## Release 0.1.2 (2021/1/1)` following command would print out `Release`.

```bash
parchant tag CHANGELOG.md "## Release "
```

## TODO
1. Better custom pattern matching with other than just second item in split.
2. Export description of any release based on pattern.
